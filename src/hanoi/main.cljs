(ns hanoi.main
  (:require [kee-frame.core :as kf]
            [hanoi.views.hanoi :as hanoi]
            [hanoi.views.intro :as intro]))

(defn app []
  [:div.app
   [kf/switch-route (fn [route] (-> route :data :name))
    :route/intro  [intro/intro-page]
    :route/hanoi [hanoi/hanoi-page]
    nil [:div "not found"]]])

(defn main! []
  (kf/start! {:routes [["/" :route/intro]
                       ["/hanoi" :route/hanoi]]
              :initial-db {}
              :root-component [app]})
  (println "loaded"))
