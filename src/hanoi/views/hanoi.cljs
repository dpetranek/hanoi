(ns hanoi.views.hanoi
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [hanoi.common :as common]))

(kf/reg-controller
 :route/hanoi
 {:params (fn [route] (when (= (-> route :data :name) :route/hanoi) true))
  :start [::initialize-hanoi]
  :stop [::destroy-hanoi]})

(defmethod common/key-handler "left-arrow"  [_] (rf/dispatch [::prev-tower]))
(defmethod common/key-handler "right-arrow" [_] (rf/dispatch [::next-tower]))
(defmethod common/key-handler "up-arrow"    [_] (rf/dispatch [::move-disk nil]))
(defmethod common/key-handler "down-arrow"  [_] (rf/dispatch [::move-disk nil]))

(rf/reg-sub
 ::tower
 (fn [db _]
   (:tower db)))

(rf/reg-sub
 ::disks
 (fn [db [_ tower]]
   (set (get db tower))))

(rf/reg-sub
 ::disk
 (fn [db _]
   (:disk db)))

(rf/reg-sub
 ::complete
 (fn [db _]
   (:complete db)))

(rf/reg-event-fx
 ::initialize-hanoi
 (fn [{:keys [db]} _]
   {:db (merge db {:t1 [7 6 5 4 3 2 1]
                   :t2 []
                   :t3 []
                   :tower :t1})}))

(rf/reg-event-fx
 ::destroy-hanoi
 (fn [{:keys [db]} _]
   {:db (dissoc db :t1 :t2 :t3 :tower :disk :complete)}))

(rf/reg-event-db
 ::select-tower
 (fn [db [_ tower]]
   (assoc db :tower tower)))

(rf/reg-event-db
 ::next-tower
 [common/mydebug]
 (fn [db _]
   (let [tower (get db :tower)
         next {:t1 :t2 :t2 :t3 :t3 :t1}]
     (assoc db :tower (get next tower)))))

(rf/reg-event-db
 ::prev-tower
 [common/mydebug]
 (fn [db _]
   (let [tower (get db :tower)
         prev {:t1 :t3 :t2 :t1 :t3 :t2}]
     (assoc db :tower (get prev tower)))))


(defn valid-move? [disk tower-disks]
  (or (empty? tower-disks)
      (< disk (peek tower-disks))))

(rf/reg-event-fx
 ::move-disk
 [common/mydebug]
 (fn [{:keys [db]} [_ clicked-tower]]
   (let [tower    (or clicked-tower (:tower db))
         complete (:complete db)]
     (if-let [selected-disk (get db :disk)]
       ;; place disk
       (let [tower-disks (get-in db [tower])]
         (if (valid-move? selected-disk tower-disks)
           {:dispatch [::check-complete]
            :db       (-> db
                          (update tower conj selected-disk)
                          (assoc :tower tower)
                          (dissoc :disk))}
           {:db db}))
       ;; select disk
       (let [disk (peek (get db tower))]
         (if (and disk (not complete))
           {:db (-> db
                    (update tower pop)
                    (assoc :disk disk)
                    (assoc :tower tower))}
           {:db db}))))))

(defn valid-full-stack [tower-disks]
  (and (= (count tower-disks) 7)
       (apply < tower-disks)))

(rf/reg-event-db
 ::check-complete
 [common/mydebug]
 (fn [db _]
   (let [t2-disks (get-in db [:t2])
         t3-disks (get-in db [:t3])]
     (cond (valid-full-stack t2-disks)
           (-> db
               (assoc :complete :t2)
               (dissoc :tower))
           (valid-full-stack t3-disks)
           (-> db
               (assoc :complete :t3)
               (dissoc :tower))
           :else
           db))))

(defn draw-disk [{:keys [size display? color class]}]
  [:div {:class class
         :style (-> {:background-color (if display? color "transparent")}
                    (cond-> (= "active-disk disk" class)
                      (assoc :width (str (* (inc size) 10) "%"))))}])

(defn tower [tower]
  (let [disks (rf/subscribe [::disks tower])
        disk (rf/subscribe [::disk])
        active-tower (rf/subscribe [::tower])]
    (fn []
      [:div.tower
       {:id tower
        :on-click #(rf/dispatch (if @disk
                                  [::common/queue [[::select-tower tower]
                                                   [::move-disk nil]]]
                                  [::move-disk tower]))}
       [draw-disk {:class "active-disk disk"
                   :size @disk
                   :display? (and @disk (= tower @active-tower))
                   :color "lightgrey"}]

       [:div.disks
        (for [slot (range 1 8)]
          ^{:key slot}
          [draw-disk {:size slot
                      :class "disk"
                      :display? (contains? @disks slot)
                      :color "black"}])]
       [:div.spindle]
       [:div.base]
       [draw-disk {:size 8
                   :class "active-tower"
                   :display? (= tower @active-tower)
                   :color "lightgrey"}]])))

(defn hanoi-page []
  (let [complete     (rf/subscribe [::complete])]
    (fn []
      [:div.hanoi {:class (if @complete "complete")}
       [:div.hanoi-title [:h2 ""]]
       (when (or (not @complete) (= @complete :t1)) [tower :t1])
       (when (or (not @complete) (= @complete :t2)) [tower :t2])
       (when (or (not @complete) (= @complete :t3)) [tower :t3])
       ])))
