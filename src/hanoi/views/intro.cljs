(ns hanoi.views.intro
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [hanoi.common :as common]))

(kf/reg-controller
 :route/intro
 {:params (fn [route] (when (= (-> route :data :name) :route/intro) true))
  :start [::initialize]})

(defonce dark-mode (atom false))

(defmethod common/key-handler "space" [_]
  (if @dark-mode
    (if-let [el (.getElementById js/document "body")]
      (do (.setAttribute el "class" "")
          (swap! dark-mode not)))
    (if-let [el (.getElementById js/document "body")]
      (do (.setAttribute el "class" "dark-mode")
          (swap! dark-mode not)))))

(rf/reg-event-fx
 ::initialize
 (fn [{:keys [db]} _]
   {:db db
    ::common/add-window-event-listener ["keydown" common/hanoi-key-handler]}))

(defn intro-page []
  [:div.hanoi.intro
   [:div.hanoi-title
    #_[:h1 "Idle Hanoi"]]
   [:div.introduction
    [:p "move the disks to another tower"]
    [:p "larger disks cannot rest atop smaller disks"]
    [:a.begin {:href (kf/path-for [:route/hanoi])} "begin"]]])
