(ns hanoi.common
  (:require [re-frame.core :as rf]
            [re-frame.interceptor :as rf-interceptor]))

(defn hdb [db] (select-keys db [:t1 :t2 :t3 :tower :disk :complete]))

(def mydebug
  (rf-interceptor/->interceptor
   :id     :mydebug
   :before (fn debug-before
             [context]
             (println "Handling event:" (get-in context [:coeffects :event]))
             context)
   :after  (fn debug-after
             [context]
             (let [event   (get-in context [:coeffects :event])
                   orig-db (get-in context [:coeffects :db])
                   new-db  (get-in context [:effects :db] ::not-found)]
               (tap> {:event  (get-in context [:coeffects :event])
                      :before orig-db
                      :after  new-db})
               (if (= new-db ::not-found)
                 (println :log "No :db changes caused by:" event)
                 (do (println "before" (hdb orig-db))
                     (println "after" (hdb new-db))))
               context))))

(rf/reg-fx
 ::add-window-event-listener
 (fn [[event-type fn]]
   (.addEventListener js/window event-type fn)))

(rf/reg-fx
 ::remove-window-event-listener
 (fn [[event-type fn]]
   (.removeEventListener js/window event-type fn)))

(rf/reg-fx
 ::dispatch-delay
 (fn [[delay queue]]
   (when (seq queue)
     (.setTimeout js/window
                  #(rf/dispatch [::queue queue])
                  delay))))

(rf/reg-event-fx
 ::queue
 [mydebug]
 (fn [{:keys [db]} [_ [action & queue]]]
   {:db db
    :dispatch action
    ::dispatch-delay [250 queue]}))

(rf/reg-event-fx
 ::noop
 [mydebug]
 (fn [{:keys [db]} _]
   {:db db}))

(def keymap
  {191 "?"
   37 "left-arrow"
   38 "up-arrow"
   39 "right-arrow"
   40 "down-arrow"
   32 "space"})

(defmulti key-handler (fn [event] (get keymap (.-which event))))
(defmethod key-handler :default [_])
(defn hanoi-key-handler [event] (key-handler event))
