(ns hanoi.solver
  (:require [hanoi.views.hanoi :as hanoi]
            [clojure.spec.alpha :as s]))

(defn solve
  "takes a state and outputs the next move"
  [state]
  (let [{:keys [t1 t2 t3 disk]} state]
    ;; If you've got a disk selected, put it down on it the next larger disk
    (cond disk
          (let [target (reduce (fn [res [tower [top-disk]]]
                                 (if (= (inc disk) top-disk)
                                   tower
                                   res))
                               nil
                               (select-keys state [:t1 :t2 :t3]))]
            [[::select-tower target]
             [::move-disk nil]])

          )))

(defn disk-sequence
  "Calculate the sequence of disk moves to move a tower of n disks (assuming three
  spindles)."
  [n]
  (cond (<= n 0) []
        (= n 1)  [1]
        :else
        (let [*-lude (disk-sequence (dec n))]
          (concat *-lude [n] *-lude))))

(defn find-disk-tower [state disk]
  (reduce (fn [res [k v]]
            (if (= (last v) disk)
              (reduced k)
              nil))
          nil
          state))

(comment
  (find-disk-tower {:t1 [3 2 1] :t2 [] :t3 []} 1)
  :t1
  (find-disk-tower {:t1 [3 2] :t2 [1] :t3 []} 2)
  :t1
  )

(defn next-valid-tower [state from moving]
  (let [destinations {:t1 :t2 :t2 :t3 :t3 :t1}
        candidate1 (get destinations from)
        candidate2 (get destinations candidate1)]
    (reduce (fn [res tower]
              (when (hanoi/valid-move? moving (get state tower))
                (reduced tower)))
            nil
            [candidate1 candidate2])))

(defn calc-next-state [state from]
  (let [moving (peek (get state from))
        to     (next-valid-tower state from moving)]
    (-> state
        (update from pop)
        (update to conj moving))))

(comment
  (calc-next-state {:t1 [3 2 1] :t2 [] :t3 []} :t1)
  {:t1 [3 2], :t2 [1], :t3 []}
  {:t1 [3 2], :t2 [], :t3 []}

  {:t1 [3 2], :t2 1, :t3 []}
  )

(defn state-sequence
  "Map a disk in the sequence to the state before that disk moves."
  [n]
  (let [initial-state {:t1 (vec (reverse (range 1 (inc n))))
                       :t2 []
                       :t3 []}]
    (reduce (fn [states disk]
              (let [current-state (last states)
                    tower (find-disk-tower current-state disk)
                    next-state (calc-next-state current-state tower)]
                (conj states next-state)))
            [initial-state]
            (disk-sequence n))))

(comment
  [{:t1 [2 1], :t2 [], :t3 []}
   {:t1 [2], :t2 [1], :t3 []}
   {:t1 [], :t2 [1], :t3 [2]}
   {:t1 [], :t2 [], :t3 [2 1]}
   ]
  (state-sequence 2)
  [{:t1 [2 1], :t2 [], :t3 []}
   {:t1 [2], :t2 [1], :t3 []}
   {:t1 [], :t2 [1], :t3 [2]}
   {:t1 [], :t2 [], :t3 [2 1]}]
  [{:t1 [2 1], :t2 [], :t3 []}
   {:t1 [2], :t2 [1], :t3 []}
   {:t1 [], :t2 [1], :t3 [2]}
   {:t1 [1], :t2 [], :t3 [2]}]
  [{:t1 [2 1], :t2 [], :t3 []}
   {:t1 [2], :t2 [1], :t3 []}
   {:t1 [], :t2 [1 2], :t3 []}
   {:t1 [], :t2 [1 2], :t3 [], nil (nil)}]
  [{:t1 [2 1], :t2 [], :t3 []}
   {:t1 [2], :t2 [1], :t3 []}
   {:t1 [2], :t2 [], :t3 [1]}
   {:t1 [2 1], :t2 [], :t3 []}]

  (state-sequence 3)
  [{:t1 [3 2 1], :t2 [], :t3 []}
   {:t1 [3 2], :t2 [1], :t3 []}
   {:t1 [3], :t2 [1], :t3 [2]}
   {:t1 [3], :t2 [], :t3 [2 1]}
   {:t1 [], :t2 [3], :t3 [2 1]}
   {:t1 [1], :t2 [3], :t3 [2]}
   {:t1 [1], :t2 [3 2], :t3 []}
   {:t1 [], :t2 [3 2 1], :t3 []}]

  (disk-sequence 2)
  (1 2 1))

(defn move-sequence
  "Calculate the sequence of disk moves to move a tower of n disks (assuming three
  spindles)."
  [n]
  (cond (<= n 0) []
        (= n 1)  [[:tower :t1] [::move-disk nil]]
        :else
        (let [*-lude (disk-sequence (dec n))]
          (concat *-lude [n] *-lude))))



(comment
  0 []
  1 [1]
  2 [1 2 1]
  3 [1 2 1 3 1 2 1]
  4 [1 2 1 3 1 2 1 4 1 2 1 3 1 2 1]

  (disk-sequence 0)
  []
  (disk-sequence -100)
  []
  (disk-sequence 1)
  [1]
  (disk-sequence 2)
  (1 2 1)
  (1 2 1)
  (disk-sequence 3)
  (1 2 1 3 1 2 1)
  (disk-sequence 4)
  (1 2 1 3 1 2 1 4 1 2 1 3 1 2 1)
  (count (disk-sequence 7))
  (1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 5 1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 6 1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 5 1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 7 1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 5 1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 6 1 2 1 3 1 2 1 4 1 2 1 3 1 2 1 5 1 2 1 3 1 2 1 4 1 2 1 3 1 2 1)


  )


(defn state-distance [state1 state2]
  )

(s/def ::disk #{7 6 5 4 3 2 1})

(s/def ::tower (s/and (s/coll-of ::disk :kind vector? :min-count 0 :max-count 7 :distinct true)
                      (fn stacked? [xs] (apply > xs))))

(s/def ::t1 ::tower)
(s/def ::t2 ::tower)
(s/def ::t3 ::tower)

(s/def ::state (s/and (s/keys :req-un [::t1 ::t2 ::t3])
                      (fn disk-only-used-once?
                        [{:keys [t1 t2 t3]}]
                        (->> (frequencies (apply concat [t1 t2 t3]))
                             (vals)
                             (reduce *)
                             (= 1)))))




(s/valid? ::tower [2 3 1])

(comment
  ;; how many possible states for 3 n-height towers?
  ;; a possible state is any arrangement with no larger numbers after smaller ones.
  ;; starting + 2(numstates)
  ;; n = 1
  ;; 1 + 2 * 1 = 3
  {:t1 [1]
   :t2 []
   :t3 []}
  {:t1 []
   :t2 [1]
   :t3 []}

  ;; n = 2
  ;; 1 + 2 * 3 = 7
  {:t1 [2 1]
   :t2 []
   :t3 []}
  {:t1 [2]
   :t2 [1]
   :t3 []}
  {:t1 []
   :t2 [1]
   :t3 [2]}
  {:t1 []
   :t2 []
   :t3 [2 1]}

  ;; n = 3
  ;; 1 + 2 * 8 = 17
  {:t1 [3 2 1]
   :t2 []
   :t3 []}
  {:t1 [3 2]
   :t2 [1]
   :t3 []}
  {:t1 [3]
   :t2 [1]
   :t3 [2]}
  {:t1 [3]
   :t2 []
   :t3 [2 1]}
  {:t1 []
   :t2 [3]
   :t3 [2 1]}
  {:t1 []
   :t2 [3 1]
   :t3 [2]}
  {:t1 [1]
   :t2 [3]
   :t3 [2]}
  {:t1 [1]
   :t2 [3 2]
   :t3 []}
  {:t1 []
   :t2 [3 2 1]
   :t3 []}

  )

(comment
  1
  2
  1
  3
  1
  2
  1

  [{:t1 [3 2 1]
    :t2 []
    :t3 []}
   {:disk 1}
   {:t1 [3 2]
    :t2 [1]
    :t3 []}
   {:disk 2}
   {:t1 [3]
    :t2 [1]
    :t3 [2]}
   {:disk 1}
   {:t1 [3]
    :t2 []
    :t3 [2 1]}
   {:disk 3}
   {:t1 []
    :t2 [3]
    :t3 [2 1]}
   {:disk 1}
   {:t1 [1]
    :t2 [3]
    :t3 [2]}
   {:disk 2}
   {:t1 [1]
    :t2 [3 2]
    :t3 []}
   {:disk 1}
   {:t1 []
    :t2 [3 2 1]
    :t3 []}

   ]

  [[:move 2 :t1]
   [:move 1 :t2 :t3]
   [:move 4 :t1 :t2]
   [:move 3 :t3 :t2]
   [:move 2 :t3 :t2]
   [:move 1 :t3 :t2]]


  (def s0 {:t1 [] :t2 [2 3] :t3 [] :disk 1})
  (conj [1 2] 3)
  [1 2 3]

  (solve s0)

  :t2
  :t1
  :t1


  (min 1 2)
  1
  (min 2 1)
  1
  (reduce min [3 1 2 ])
  1




  )
